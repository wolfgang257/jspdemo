<%-- 
    Document   : index
    Created on : 9/07/2017, 02:04:55 PM
    Author     : josorio
--%>
<%@page import="java.util.Date"  %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="about.jsp" %>
        ó
        <jsp:include page="about.jsp"/>
        
        <%-- Comentarios JSP que no se veran en el html --%>
        <p>-------------------</p>
        
        <h1>Fecha mediante directiva</h1>
        <p><%=new Date()%></p>
        
        <p>Fecha mediante varialbe local</p>
        <% Date fecha = new Date(); %>
        <% out.println("<i>"+fecha+"</i>");%>
        
        <p>Fecha formateada</p>
        <c:set var="fecha" value="<%=new Date()%>" />
        <fmt:formatDate value="${fecha}" type="both"/>
        
        <%
            String[] vehiculos = new String[]{"coche", "moto", "avion"};
            pageContext.setAttribute("vehiculos", vehiculos);
        %>
        <c:forEach var="vehiculo" items="${pageScope.vehiculos}">
            <p>
                <c:out value="${vehiculo}"/> -> <c:out value="${fn:toUpperCase(vehiculo)}"/>
            </p>
        </c:forEach>
            
        <p>-------------------</p>
            
        <form action="greeter.jsp" method="POST">
            <p>Tu nombre: <input type="text" name="nombre" size="20"/></p>
            <p><input type="submit"/></p>
        </form>    
    </body>
</html>
